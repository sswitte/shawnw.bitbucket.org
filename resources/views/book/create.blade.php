@extends('layouts.app')

@section('title')
	Nieuw boek toevoegen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
    <a href="http://books.google.com" target="_blank">Klik hier voor google books!</a>
    {!! Form::open(['route' => ['book.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-6">
		{!! Form::label('isbn', 'ISBN', ['class' => 'control-label']) !!}
		{!! Form::text('isbn', null, ['class' => 'form-control', 'placeholder' => 'ISBN Nummer']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('title', 'Titel', ['class' => 'control-label']) !!}
		{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'De titel hier']) !!}


</div>

<div class="form-group">
	<div class="col-sm-12">
		<br>
		<button type="submit" class="btn btn-primary">
			Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection
@section('scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script src="{{ URL::asset('js/main.js') }}"></script>

@endsection