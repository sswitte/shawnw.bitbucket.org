function bookSearch(){
    var search = $('#isbn').val();
    //document.getElementById('results').innerHTML = ""
    //console.log(search);

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=isbn:" + search,
        dataType: "json",
        type: 'GET',
        success: function(data) {
            console.log(data);
            for(i = 0; i < data.items.length; i++){
                //document.getElementById('titel').value = data.items[i].volumeInfo.title;
                $('#title').val(data.items[i].volumeInfo.title);
            }
        }
    });
}

document.getElementById('isbn').addEventListener('change', bookSearch, false);